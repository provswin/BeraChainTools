# from eth_account import Account
# from loguru import logger

# from bera_tools import BeraChainTools

# # 创建5000个钱包
# accounts = [Account.create() for _ in range(5000)]

# # 将account.address写入到address.txt文件中
# with open('address.txt', 'w') as f:
#     for account in accounts:
#         f.write(account.address + '\n')
#         logger.debug(f'address:{account.address}')

# # 将account.key.hex()写入到address_privatekey.txt文件中
# with open('address_privatekey.txt', 'w') as f:
#     for account in accounts:
#         f.write(account.key.hex() + '\n')
#         logger.debug(f'key hex:{account.key.hex()}')



# bera = BeraChainTools(private_key='0x0e4ccddb7e1c3b58977a7ebde75ff187c0ed3205d6875cb7e7b976bd1e8fccc6', client_key='442e93799afcfc1958cf9b6ec8b6f7260f99117640101',solver_provider='yescaptcha',rpc_url='https://rpc.ankr.com/berachain_testnet')
# 使用2captcha solver googlev3
# bera = BeraChainTools(private_key=account.key, client_key=client_key,solver_provider='2captcha',rpc_url='https://rpc.ankr.com/berachain_testnet')
# 使用ez-captcha solver googlev3
# bera = BeraChainTools(private_key=account.key, client_key=client_key,solver_provider='ez-captcha',rpc_url='https://rpc.ankr.com/berachain_testnet')

# 不使用代理
# result = bera.claim_bera()
# 使用代理
# result = bera.claim_bera(proxies={'http':"http://120.43.35.164:15001"})
# logger.debug(result.text)
import re

# # 打开并读取文件
# with open('20240422.txt', 'r') as file:
#     data = file.read()

# # 使用正则表达式匹配所有满足规则的字符串
# matches = re.findall(r'Added (0x[a-fA-F0-9]{40}) to the queue', data)

# # 使用set去除重复的字符串
# unique_matches = set(matches)

# # 打印所有不重复的字符串
# for match in unique_matches:
#     print(match)


# response_text = '{"msg":"Added 0x551144e0F336ACFfe76e505576035e4fE7B4E599 to the queue"}'
# if 'try again' not in response_text and ('message":"' in response_text or ' to the queue'):
#     print('in the queue')
# else:
#     print('not in the queue')


# 打开并读取claim_success.txt文件
with open('claim_success.txt', 'r') as file:
    claim_success_lines = file.readlines()

# 打开并读取address.txt文件
with open('address.txt', 'r') as file:
    address_lines = file.readlines()

# 打开address_privatekey.txt文件
with open('address_privatekey.txt', 'r') as file:
    privatekey_lines = file.readlines()

# 对于claim_success.txt文件中的每一行
for claim_line in claim_success_lines:
    # 去除行尾的换行符
    claim_line = claim_line.strip()

    # 在address.txt文件中查找匹配的行
    for i, address_line in enumerate(address_lines):
        # 去除行尾的换行符
        address_line = address_line.strip()

        # 如果找到匹配的行
        if claim_line == address_line:
            # 打印对应行数的数据
            print(privatekey_lines[i])